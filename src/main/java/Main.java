/**
 * Main class of this app
 */
public final class Main {
    
    /**
     * utility class - utility constructor
     */
    private Main() {
    }

    public static void main(final String[] args) {
        NumberUtils.printNumbersForInterval(-9, 10);
        NumberUtils.printBiggestFibonacciNumbers(10);
    }
}
