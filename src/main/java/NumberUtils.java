import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Utility class that contains logic for work with ood, even numbers
 * and fibonacci numbers.
 *
 * @author Dmytro Vusyk
 * @version 1.0.0
 */
public final class NumberUtils {

    /**
     * Integer constant that implement 100 Percent.
     */
    private static final Integer HUNDRED_PERCENT = 100;

    /**
     * This is Utility method. Constructor must be private.
     */
    private NumberUtils() {
    }

    /**
     * This method prints all of the specific tasks with numbers we do into console.
     *
     * @param begin first number in array
     * @param end   last number in array
     */
    public static void printNumbersForInterval(final int begin, final int end) {
        validateInputData(begin, end);
        ArrayList<Integer> integers = getNumbers(begin, end);
        printOddNumbersAsc(integers);
        printEvenNumbersDesc(integers);
        printSumOfOddNumbers(integers);
        printSumOfEvenNumbers(integers);
    }

    /**
     * This Method prints into console biggest odd number and biggest even number
     * among our Fibonacci numbers.
     *
     * @param size count of Fibonacci numbers
     */
    public static void printBiggestFibonacciNumbers(final int size) {
        ArrayList<Integer> fibonacci = buildFibonacciNumbers(size);
        System.out.println("Fibonacci: " + fibonacci.toString());
        printBiggestFibonacciOddNumber(fibonacci);
        printBiggestFibonacciEvenNumber(fibonacci);
        System.out.println("Percentage of odd numbers: "
                + getPercentageOfOddNumbers(fibonacci));
        System.out.println("Percentage of even numbers: "
                + getPercentageOfEvenNumbers(fibonacci));
    }

    /**
     * This method builds an ArrayList with Fibonacci numbers.
     *
     * @param size count of Fibonacci numbers
     * @return ArrayList with Fibonacci numbers
     */
    private static ArrayList<Integer> buildFibonacciNumbers(final int size) {
        ArrayList<Integer> fibonacci = new ArrayList<>();
        int n0 = 1;
        int n1 = 1;
        int n2;
        fibonacci.add(n0);
        fibonacci.add(n1);
        for (int i = 3; i <= size - 2; i++) {
            n2 = n0 + n1;
            fibonacci.add(n2);
            n0 = n1;
            n1 = n2;
        }
        return fibonacci;
    }

    /**
     * This method calculates percentage of Odd numbers among Fibonacci numbers.
     *
     * @param fibonacci ArrayList with fibonacci numbers
     * @return String with formatted value of double with 4 signs after coma
     */
    private static String getPercentageOfOddNumbers(final ArrayList<Integer> fibonacci) {
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        double countOfOddNumbers = fibonacci.stream()
                .filter(n -> n % 2 != 0)
                .collect(Collectors.toCollection(ArrayList::new)).size();
        double countOfNumbers = fibonacci.size();
        double percent = HUNDRED_PERCENT / countOfNumbers * countOfOddNumbers;
        return df.format(percent);
    }

    /**
     * This method calculates percentage of Even numbers among Fibonacci numbers.
     *
     * @param fibonacci ArrayList with fibonacci numbers
     * @return String with formatted value of double with 4 signs after point
     */
    private static String getPercentageOfEvenNumbers(final ArrayList<Integer> fibonacci) {
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        double countOfEvenNumbers = fibonacci.stream()
                .filter(n -> n % 2 == 0)
                .collect(Collectors.toCollection(ArrayList::new)).size();
        double countOfNumbers = fibonacci.size();
        double percent = HUNDRED_PERCENT / countOfNumbers * countOfEvenNumbers;
        return df.format(percent);
    }

    /**
     * This method prints the biggest odd Fibonacci number in ArrayList.
     *
     * @param fibonacci ArrayList with Fibonacci numbers
     */
    private static void printBiggestFibonacciOddNumber(final ArrayList<Integer> fibonacci) {
        Optional<Integer> oddMax = fibonacci.stream()
                .filter(n -> n % 2 != 0)
                .reduce(Integer::max);
        oddMax.ifPresent(integer ->
                System.out.println("biggest odd number F1: " + integer.toString()));
    }

    /**
     * This method prints the biggest even Fibonacci number in ArrayList.
     *
     * @param fibonacci ArrayList with Fibonacci numbers
     */
    private static void printBiggestFibonacciEvenNumber(final ArrayList<Integer> fibonacci) {
        Optional<Integer> evenMax = fibonacci.stream()
                .filter(n -> n % 2 == 0)
                .reduce(Integer::max);
        evenMax.ifPresent(integer
                -> System.out.println("biggest even number F2: " + integer.toString()));
    }

    /**
     * This method prints all odd numbers among Fibonacci numbers
     * in natural order.
     *
     * @param numbers ArrayList with Fibonacci numbers
     */
    private static void printOddNumbersAsc(final ArrayList<Integer> numbers) {
        ArrayList<Integer> result = numbers.stream()
                .filter(n -> n % 2 != 0)
                .sorted()
                .collect(Collectors.toCollection(ArrayList::new));
        System.out.println("odd numbers from start to end: "
                + result.toString());
    }

    /**
     * This method prints all even numbers among Fibonacci numbers
     * in natural order.
     *
     * @param numbers ArrayList with Fibonacci numbers
     */
    private static void printEvenNumbersDesc(final ArrayList<Integer> numbers) {
        ArrayList<Integer> result = numbers.stream()
                .filter(n -> n % 2 == 0)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toCollection(ArrayList::new));
        System.out.println("even numbers from end to start: "
                + result.toString());
    }

    /**
     * This method prints the sum of all odd numbers among Fibonacci numbers.
     *
     * @param numbers ArrayList with Fibonacci numbers
     */
    private static void printSumOfOddNumbers(final ArrayList<Integer> numbers) {
        Optional<Integer> oddSum = numbers.stream()
                .filter(n -> n % 2 != 0)
                .sorted()
                .reduce(Integer::sum);
        oddSum.ifPresent(integer ->
                System.out.println("sum of the all odd numbers: "
                        + integer.toString()));
    }

    /**
     * This method prints the sum of all even numbers among Fibonacci numbers.
     *
     * @param numbers ArrayList with Fibonacci numbers
     */
    private static void printSumOfEvenNumbers(final ArrayList<Integer> numbers) {
        Optional<Integer> evenSum = numbers.stream()
                .filter(n -> n % 2 == 0)
                .sorted()
                .reduce(Integer::sum);
        evenSum.ifPresent(integer ->
                System.out.println("sum of the even numbers: "
                        + integer.toString()));
    }

    /**
     * This method builds an ArrayList with integers from begin to end.
     *
     * @param begin first integer in row
     * @param end   last integer in row
     * @return ArrayList with integers from begin to end
     */
    private static ArrayList<Integer> getNumbers(final int begin, final int end) {
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = begin; i <= end; i++) {
            numbers.add(i);
        }
        return numbers;
    }

    /**
     * This method validates input data.
     *
     * @param begin first integer in row
     * @param end   last integer in row
     * @throw IllegalArgumentException if end is less then begin
     */
    private static void validateInputData(final int begin, final int end) {
        if (end < begin) {
            String errorMassage = "Begin is greater then end of array";
            throw new IllegalArgumentException(errorMassage);
        }
    }

}
